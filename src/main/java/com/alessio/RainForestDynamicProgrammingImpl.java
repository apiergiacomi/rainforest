package com.alessio;

/**
 * This solution adopts dynamic programming, populating a matrix containing the max
 * treasure you can collect in each cell starting from the there and terminating at the bottom-right corner,
 * and populating it backwards, starting from the bottom right corner and iterating each row from right to left,
 * from the last row to the first one.
 *
 * By the time you calculate C(i,j), you are guaranteed to have calculated both the max treasure collected
 * in the cell on the right, and in the cell on the bottom, if indeed such cells exist and you are not on
 * the bottom/right edge of the matrix already).
 *
 * Assumptions:
 * 1 ) all the treasure values are positive. I'll use Integer.MIN_VALUE in my dynamic programming matrix
 * to represent an invalid path. I could have used null as in the original map, but using an integer lets me leverage Math.max().
 * 2) You can't provide in input a matrix with an empty row, if a row is there, it must contain N>1 elements.
 *  I.e. [[]] is not a possible input, [] is.
 *
 * Time complexity and space complexity are O(N*M)
 *
 */
public class RainForestDynamicProgrammingImpl implements RainForest {

    @Override
    public int findMaxCollectibleTreasure(Integer[][] map) {
        if (map == null || map.length == 0) {
            return 0;
        }

        int rows = map.length;
        int columns = map[0].length;

        // optimalPathTreasure will contain the max amount of treasure that can be collected
        // arriving from the bottom left corner and starting at each cell.
        int[][] optimalPathTreasure = new int[rows][columns];

        for (int i = rows-1; i >=0; i--) {
            for (int j = columns-1; j >=0; j--) {

                // skipping the rivers
                if (map[i][j] == null) {
                    optimalPathTreasure[i][j] = Integer.MIN_VALUE;
                    continue;
                }

                // initialising the bottom-right corner of the matrix
                if (j == columns -1 && i == rows-1) {
                    optimalPathTreasure[i][j] = map[i][j];
                    continue;
                }

                optimalPathTreasure[i][j] = Math.max(
                    treasureFromBottomPath(optimalPathTreasure, map, i, j),
                    treasureFromLeftPath(optimalPathTreasure, map, i, j)
                );
            }
        }

        return Math.max(0, optimalPathTreasure[0][0]);
    }

    // Design tradeoff: passing the maps rather than making them fields. This is thread safe but the OO design isn't the best.
    private int treasureFromBottomPath(int[][] optimalPathTreasure, Integer[][] map, int i, int j) {
        int treasureFromBottom = Integer.MIN_VALUE;
        // Moving from the top should be a permitted move:
        // - current row i can't be the last one;
        // - the cell to the top of i,j can't contain a river (Integer.MIN_VALUE)
        if (i < optimalPathTreasure.length - 1 && optimalPathTreasure[i+1][j] != Integer.MIN_VALUE) {
            treasureFromBottom = optimalPathTreasure[i+1][j] + map[i][j] ;
        }
        return treasureFromBottom;
    }

    // Design tradeoff: passing the maps rather than making them fields. This is thread safe but the OO design isn't the best.
    private int treasureFromLeftPath(int[][] optimalPathTreasure, Integer[][] map, int i, int j) {
        int treasureFromLeft = Integer.MIN_VALUE;
        // Moving from left should be a permitted move:
        // - current column j can't be the last one;
        // - the cell to the right of i,j can't contain a river (Integer.MIN_VALUE)
        if (j < optimalPathTreasure[i].length -1 && optimalPathTreasure[i][j+1] != Integer.MIN_VALUE) {
            treasureFromLeft = optimalPathTreasure[i][j+1] + map[i][j];
        }
        return treasureFromLeft;
    }

}
