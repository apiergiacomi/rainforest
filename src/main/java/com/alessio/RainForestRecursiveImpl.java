package com.alessio;

/*
 *
 * Time complexity: I'm not sure, I think something smaller but not far from 2^(N+M)
 *
 * Space complexity: O(N+M), the recursion can be at most N+M-1 deep
 *
 */
public class RainForestRecursiveImpl implements RainForest {

    @Override
    public int findMaxCollectibleTreasure(Integer[][] map) {
        if (map == null || map.length == 0) {
            return 0;
        }

        Integer maxCollectedTreasure = findMaxCollectibleTreasure(map, 0, 0, 0);

        return maxCollectedTreasure != null ? maxCollectedTreasure : 0;
    }

    private Integer findMaxCollectibleTreasure(Integer[][]map, int startRow, int startColumn, int treasureCollected) {

        if (map[startRow][startColumn] == null) {
            return null;
        }

        int lastColumn = map[0].length - 1;
        int lastRow = map.length - 1;

        // The final destination has been reached.
        if (startColumn == lastColumn && startRow == lastRow) {
            return map[lastRow][lastColumn] == null ? null : treasureCollected + map[lastRow][lastColumn];
        }

        Integer maxTreasureRight = maxTreasureCollectedGoingRight(map, startRow, startColumn, treasureCollected);

        Integer maxTreasureLeft = maxTreasureCollectedGoingLeft(map, startRow, startColumn, treasureCollected);

        if (maxTreasureLeft == null) {
            return maxTreasureRight;
        }

        if (maxTreasureRight == null) {
            return maxTreasureLeft;
        }

        return  Math.max(maxTreasureLeft, maxTreasureRight);
    }

    private Integer maxTreasureCollectedGoingRight(Integer[][] map, int startRow, int startColumn, int treasureCollected) {

        if (startColumn < map[0].length - 1 && map[startRow][startColumn+1] != null) {
            return findMaxCollectibleTreasure(map, startRow, startColumn+1,treasureCollected+map[startRow][startColumn]);
        }
        return null;
    }


    private Integer maxTreasureCollectedGoingLeft(Integer[][] map, int startRow, int startColumn, int treasureCollected) {

        if (startRow < map.length - 1 && map[startRow+1][startColumn] != null) {
            return findMaxCollectibleTreasure(map, startRow+1, startColumn,treasureCollected+map[startRow][startColumn]);
        }
        return null;
    }

}
