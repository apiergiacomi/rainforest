package com.alessio;

public interface RainForest {
    public int findMaxCollectibleTreasure(Integer[][]map);
}