package com.alessio;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RainForestRecursiveImplTest {

    RainForest rf = new RainForestRecursiveImpl();

    @Test
    public void providedTestCase()
    {
        Integer[][] map = {
                {1,     1,      2,      1,      1},
                {1,     null,   4,      null,   2},
                {4,     1,      1,      null,   1},
                {null,  null,   1,      1,      1}
        };
        assertEquals(12, rf.findMaxCollectibleTreasure(map));
    }

    @Test
    public void edgeCases() {
        assertEquals(0, rf.findMaxCollectibleTreasure(null));
        assertEquals(0, rf.findMaxCollectibleTreasure(new Integer[][]{}));
        assertEquals(0, rf.findMaxCollectibleTreasure(new Integer[][]{{1,null, 1}}));
    }

    @Test
    public void someCases()
    {
        Integer[][] map = {
                {1,     1,      2,      1,      1},
        };
        assertEquals(6, rf.findMaxCollectibleTreasure(map));

        map[0][3] = null;
        assertEquals(0, rf.findMaxCollectibleTreasure(map));
    }

    @Test
    public void moreCases()
    {
        Integer[][] map = {
                {1,     1,      2,      1,      1},
                {4,     1,      1,      1,      1},
        };
        assertEquals(9, rf.findMaxCollectibleTreasure(map));

        map[1][0] = null;
        assertEquals(7, rf.findMaxCollectibleTreasure(map));

        map[0][0] = null;
        assertEquals(0, rf.findMaxCollectibleTreasure(map));
    }

}
